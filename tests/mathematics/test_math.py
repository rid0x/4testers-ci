import time


def test_if_two_is_equal_to_one_plus_one():
    time.sleep(2)
    assert 2 == 1 + 1


def test_incorrect_calculation():
    time.sleep(3)
    assert 2 > 5
